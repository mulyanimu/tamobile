package com.example.mulyani.tamobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnSignIn, btnSignOut;
    EditText edtUser, edtPass;

    String username, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = edtUser.getText().toString();
                password = edtPass.getText().toString();

                if ( username.equals("mulyani") && password.equals("mul")){
                    Toast.makeText(MainActivity.this, "berhasil login", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this,HomeActivity.class));
                }
            }
        });
    }

    private void initView() {
        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        btnSignOut = (Button) findViewById(R.id.btn_sign_up);

        edtPass = (EditText) findViewById(R.id.edtPassword);
        edtUser = (EditText) findViewById(R.id.edtUsername);
    }


}
